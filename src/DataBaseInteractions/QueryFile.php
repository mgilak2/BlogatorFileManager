<?php

namespace Blogator\Components\Comments;

use Blogator\Components\FileManager\Contracts\FilesDataBaseBridge;

class QueryFile
{
    /**
     * @var FilesDataBaseBridge
     */
    private $dataBaseBridge;

    public function __construct(FilesDataBaseBridge $dataBaseBridge)
    {
        $this->dataBaseBridge = $dataBaseBridge;
    }

    public function getCommentByPost($postId)
    {

    }

    public function getCommentById($commentId)
    {

    }

    public function getCommentByAuthor($authorId)
    {

    }

    public function deleteMany(array $commentsIds = [])
    {

    }

    public function notSpams(array $commentsIds = [])
    {

    }

    public function spams(array $commentsIds = [])
    {
    }

    public function softDeletes(array $commentsIds = [])
    {

    }

    public function undoSoftDeletes(array $commentsIds = [])
    {

    }

    public function replyToComment()
    {

    }

    public function replyToPost()
    {

    }
}