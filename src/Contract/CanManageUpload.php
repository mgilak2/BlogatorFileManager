<?php

namespace Blogator\Components\FileManager\Contracts;

use Symfony\Component\HttpFoundation\File\UploadedFile;

interface CanManageUpload
{
    public function put(UploadedFile $file, $whereToPut);
}