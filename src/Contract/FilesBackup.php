<?php

namespace Blogator\Components\FileManager\Contracts;

interface FilesBackup
{
    /**
     * @return bool
     */
    public function hasAnySpecialFolderOfYours();
}