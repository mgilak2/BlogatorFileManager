<?php

namespace Blogator\Components\FileManager;

use Blogator\Components\Contracts\BlogatorComponent;
use Illuminate\Support\ServiceProvider;

class FileManagerServiceProvider extends ServiceProvider implements BlogatorComponent
{
    public function register()
    {
    }
}