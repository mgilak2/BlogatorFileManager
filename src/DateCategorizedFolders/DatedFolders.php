<?php

namespace Blogator\Components\FileManager\DateCategorizedFolders;

class DatedFolders
{
    protected $baseDirection;

    public function __construct($base)
    {
        $this->baseDirection = $base;
    }

    public function getDirection()
    {
        return $this->baseDirection . "/"
        . date("Y/m/d");
    }

    public function create()
    {
        $directions = $this->todayAsFolderDirections();
        $this->makeDirectory($directions);
        return $this;
    }

    private function todayAsFolderDirections()
    {
        $day = date("Y/m/d");
        $month = date("Y/m");
        $year = date("Y");
        return [$year, $month, $day];
    }

    private function makeDirectory($directions)
    {
        foreach ($directions as $directory)
            if (!is_dir($this->baseDirection . "/" . $directory))
                mkdir($this->baseDirection . "/" . $directory);
    }
}