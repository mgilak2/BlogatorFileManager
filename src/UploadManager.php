<?php

namespace Blogator\Components\FileManager;

use Blogator\Components\FileManager\Contracts\CanManageUpload;
use Illuminate\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class UploadManager implements CanManageUpload
{
    /**
     * @var Filesystem
     */
    private $file;

    public function __construct(Filesystem $file)
    {
        $this->file = $file;
    }

    public function put(UploadedFile $file, $whereToPut)
    {
        $this->file->move($file->getRealPath(),
            $whereToPut . "/" . $file->getFilename());
    }
}