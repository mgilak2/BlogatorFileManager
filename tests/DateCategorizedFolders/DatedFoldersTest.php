<?php

use Blogator\Components\FileManager\DateCategorizedFolders\DatedFolders;

class DatedFoldersTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var DatedFolders
     */
    protected $datedFolder;

    protected $baseFolder = __DIR__ . "/upload";

    public function setUp()
    {
        $this->createBaseUploadFolder();
        $this->datedFolder = new DatedFolders($this->baseFolder);
    }

    private function createBaseUploadFolder()
    {
        mkdir($this->baseFolder);
    }

    public function testCreate()
    {
        $this->datedFolder->create();
        $this->assertTrue(
            is_dir($this->baseFolder . "/" . date("Y/m/d"))
        );
    }

    public function tearDown()
    {
        parent::tearDown();
        $this->removeAllCreatedFiles();
    }

    private function removeAllCreatedFiles()
    {
         system("rm -rf " . escapeshellarg($this->baseFolder));
    }
}