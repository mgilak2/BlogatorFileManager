<?php

use Blogator\Components\FileManager\UploadManager;
use Illuminate\Filesystem\Filesystem;
use Mockery as m;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class UploadManagerTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var UploadManager
     */
    protected $upload;
    /**
     * @var \Mockery\MockInterface
     */
    protected $mockFile;


    public function setUp()
    {
        $this->createAnInstanceOfUpload();
    }

    private function createAnInstanceOfUpload()
    {
        $this->mockFile = m::mock(Filesystem::class);
        $this->upload = new UploadManager($this->mockFile);
    }

    public function testPut()
    {
        $mockUploadedFile = m::mock(UploadedFile::class);
        $mockUploadedFile->shouldReceive("getRealPath")->once()->andReturn("someStringAddressingFileIntoTmpFolder");
        $mockUploadedFile->shouldReceive("getFilename")->once()->andReturn("some.png");

        $this->mockFile->shouldReceive("move")
            ->with("someStringAddressingFileIntoTmpFolder",
                "uploads/2015/2/02/some.png")->once();

        $this->upload->put($mockUploadedFile, "uploads/2015/2/02");
    }

    public function tearDown()
    {
        parent::tearDown();
        $this->checkMocksBehavior();
    }

    private function checkMocksBehavior()
    {
        m::close();
    }
}